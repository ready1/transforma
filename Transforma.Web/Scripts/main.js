var debug = false;

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-center",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

var currentSideMenu = "";

var myEvents = {
    gpsToggled: "gpsToggled",
    characterUpdated: "characterUpdated"
}

var actions = {
    moving: false
}

var character = {
    CharacterId: "",
    Name: "",
    Transport: "walking",
    Money: 100000,
    Health: 5,
    Education: 5,
    Strength: 5,
    Charisma: 5,
    Luck: 5,
    Travelled: 0,
    Lat: 0,
    Lng: 0,
    GpsEnabled: false
}

var colours = {
    character: "#0047d6",
    bus: "#ede74b",
    house: "#0fe024",
    controls: "#ed4259"
}

var controls = {
    scale: 0.7
}

var securityCheck = 0;

var previousLocation = null;

var properties = [];

$(document).ready(function () {
    $("#gps").click(function () {
        character.GpsEnabled = $("#gps").is(':checked');
        saveCharacter();
        $(document).trigger(myEvents.gpsToggled, character);
    });

    $(document).bind(myEvents.characterUpdated, function () { updateCharacterView(); });

    $("#buyHome").click(buyHome);
});

function buyHome() {

    $('#confirmBuyHouse').modal();
}

function confirmBuyHome() {
    $('#confirmBuyHouse').modal('hide');

    var property = {
        CharacterId: character.CharacterId,
        Size: 0,
        Name: "My House",
        Lat: character.Lat,
        Lng: character.Lng
    }

    $.ajax({
        url: '/api/Properties/',
        data: property,
        type: 'POST',
        success: function (response) {
            toastr.success('You purchased a new home');
            getPropertiesByLocation();
        }
    });
}

function getMyProperties() {
    $.getJSON("/action/GetPropertiesByCharacterId")
        .done(function (data) {
            if (data) {
                properties = JSON.parse(data.result);
            }
        });
}

function getCharacter(id) {
    $.getJSON("/api/Characters/" + id)
    .done(function (data) {
        if (data) {
            character = data;
            updateCharacterView();
            getMyProperties();
            $(document).trigger(myEvents.gpsToggled, character);
            $(document).trigger(myEvents.characterUpdated, character);
        }
    });
}

function saveCharacter() {
    $.ajax({
        url: '/api/Characters/' + character.CharacterId,
        data: character,
        type: 'PUT',
        success: function (response) {
        }
    });
}

function setCharacter(c) {
    $(document).trigger(myEvents.characterUpdated, character);
}

function makeSomeMoney() {
    $.getJSON("/Action/MakeMoney/")
    .done(function (data) {
        if (data) {
            console.log(data);
            character.Money += eval(data.result);
            toastr.success('You made $' + data.result);
            $(document).trigger(myEvents.characterUpdated, character);
        }
    });
}

function checkSecurity() {
    $.getJSON("/Action/CheckSecurity/")
    .done(function (data) {
        if (data) {
            console.log(data);
            if (data.result === "0") return;

            character.Money -= eval(data.result);
            toastr.error('You were robbed $' + data.result);
            $(document).trigger(myEvents.characterUpdated, character);
        }
    });
}

function updateCharacterView() {
    $('#gps').prop('checked', character.GpsEnabled);
    $("#money").html("$" + character.Money);
    $("#transport").html(character.Transport);
    $("#health").html(character.Health);
    $("#education").html(character.Education);
    $("#strength").html(character.Strength);
    $("#charisma").html(character.Charisma);
    $("#luck").html(character.Luck);
}

function changeMenu(menuName) {
    if(currentSideMenu.length > 0)
        $('#' + currentSideMenu).offcanvas('hide');

    currentSideMenu = menuName;

    $('#' + currentSideMenu).offcanvas('show');
}

function closeOtherMenu(currentMenu) {
    if (currentMenu !== 'character')
        $('#character').offcanvas('hide');
    if (currentMenu !== 'inventory')
        $('#inventory').offcanvas('hide');
    if (currentMenu !== 'assets')
        $('#assets').offcanvas('hide');

    $('#' + currentMenu).offcanvas('toggle');
}

function rad(x) {
    return x * Math.PI / 180;
};

function getDistance(p1, p2) {
    return geolib.getDistance(
        { latitude: p1.Lat, longitude: p1.Lng },
        { latitude: p2.Lat, longitude: p2.Lng }
    );
};