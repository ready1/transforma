﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Transforma.Web.Models;

namespace Transforma.Web.Controllers
{
    public class ActionController : Controller
    {
        private TransformaDataEntities db = new TransformaDataEntities();
        private Random rand = new Random(DateTime.Now.Second);
        // GET: Action
        public JsonResult MakeMoney()
        {
            rand = new Random(DateTime.Now.Second);
            // randomly make some money
            // depending on the area you are in and the
            // education level of your character
            var character = db.Characters.Find(User.Identity.GetUserId());

            if (character == null)
            {
                return Json(new { result = JsonConvert.SerializeObject(0) }, JsonRequestBehavior.AllowGet);
            }

            var money = rand.Next(50);
            money += character.Education.Value * money;


            try
            {
                // increase the character
                character.Money += money;
                db.Entry(character).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception)
            {
            }

            return Json(new { result = JsonConvert.SerializeObject(money) }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckSecurity()
        {
            rand = new Random(DateTime.Now.Second);
            // randomly get robbed
            // depending on the safety of area you are in and the
            // strength level of your character
            var character = db.Characters.Find(User.Identity.GetUserId());

            if (character == null)
            {
                return Json(new { result = JsonConvert.SerializeObject(0) }, JsonRequestBehavior.AllowGet);
            }

            // chances of getting robbed increases depending on the area
            var areaSafety = 80;
            var chance = (rand.Next(100)) > areaSafety;

            if (chance)
            {
                var money = rand.Next(100);
                money -= character.Strength.Value;

                try
                {
                    // decrease the character
                    character.Money -= money;
                    db.Entry(character).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception)
                {
                }

                return Json(new { result = JsonConvert.SerializeObject(money) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { result = JsonConvert.SerializeObject(0) }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPropertiesByCharacterId()
        {
            return Json(new { result = JsonConvert.SerializeObject(db.GetPropertiesByCharacterId(User.Identity.GetUserId())) }, JsonRequestBehavior.AllowGet);
        }
    }
}