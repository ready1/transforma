﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Transforma.Web.Models;

namespace Transforma.Web.Controllers
{
    public class BusStopsController : ApiController
    {
        private TransformaDataEntities db = new TransformaDataEntities();

        // GET: api/BusStops/GetBusStopsByLocation
        public IEnumerable<BusStop> GetBusStopsByLocation(double lat, double lng)
        {
            return db.GetBusStopsByLocation(lat, lng);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BusStopExists(int id)
        {
            return db.BusStops.Count(e => e.BusStopId == id) > 0;
        }
    }
}