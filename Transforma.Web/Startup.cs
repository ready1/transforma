﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Transforma.Web.Startup))]
namespace Transforma.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
